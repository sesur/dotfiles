(let* ((init-base (concat user-emacs-directory "config"))
       (init-el (concat init-base ".el"))
       (init-org (concat init-base ".org")))
  (if (file-exists-p init-el)
      (load init-el)
    (progn
      (require 'org)
      (org-babel-tangle-file init-org init-el)
      (load init-el))))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(geiser-company guix geiser-guile avy tree-sitter-langs tree-sitter-indent tree-sitter winum ws-butler flycheck lsp-ivy lsp-mode zenburn-theme which-key use-package undo-tree tuareg smartparens rainbow-delimiters powerline org-projectile nlinum multiple-cursors magit lsp-ui ivy-rich hydra helpful haskell-mode glsl-mode ggtags general geiser function-args exec-path-from-shell evil-tutor doom-themes doom-modeline dired-single dired-hide-dotfiles counsel-projectile company cider all-the-icons-dired ace-jump-mode ac-slime)))
